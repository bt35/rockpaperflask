from flask import Flask, render_template, flash, request
from classes import rockPaperClass as rClass

app = Flask(__name__)

rps = rClass.RockPaperScissors()


@app.route("/", methods=["GET", "POST"])
def game():

    output = ""
    impossibleOutput = "Impossible Mode Enabled. Prepare for infinite defeat."
    # Send choices to rock paper scissors class.
    if request.form["submit_button"] == "Rock":
        output = rps.player("rock", rps.CHOICES)
    elif request.form["submit_button"] == "Paper":
        output = rps.player("paper", rps.CHOICES)
    elif request.form["submit_button"] == "Scissors":
        output = rps.player("scissors", rps.CHOICES)
    elif request.form["submit_button"] == "Impossible Mode":
        if rps.IMPOSSIBLE_MODE == False:
            rps.IMPOSSIBLE_MODE = True
            impossibleOutput = "Impossible Mode Enabled. Prepare for infinite defeat."
        else:
            rps.IMPOSSIBLE_MODE = False
            impossibleOutput = "Impossible mode disabled. Enable if you dare."
        # Not implemented yet.
    elif request.form["submit_button"] == "New Game?":
        rps.newGame()
        impossibleOutput = "Impossible mode disabled. Enable if you dare."

    else:
        # Error output here, I think.
        output = "something"

    # Always output the score... do not ask for input.
    s = rps.returnScores()
    playerScore = s[0]
    AIScore = s[1]

    print(rps.IMPOSSIBLE_MODE)

    return render_template(
        "game.html",
        output=output,
        player=playerScore,
        AI=AIScore,
        impossibleMode=impossibleOutput,
    )
