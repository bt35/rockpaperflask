# What is it?

Rock Paper Scissors on Flask. There's a command line version of the game too. 

# TODO:
- There is a logic bug in there somewhere, sometimes the wrong user wins in non impossible mode. The commandline version has this bug too. 
- Maybe a unit test? 
- Mabye plot the wins versus losses over time and reload it on flask template! 
- Remove the debug print statements. 
