"""Rock Paper Scissors Game"""
from random import choice
from sys import exit
import time


def AI(pick):
    """The AI should pick at random from CHOICES"""
    pk = choice(list(pick))
    return pk


def impossibleModeAI(player, CHOICES):
    """Stub or impossible mode."""
    # The AI should cheat and read the players choice!
    if player == "rock":
        return "paper"
    if player == "paper":
        return "scissors"
    if player == "scissors":
        return "rock"


def player(pick, CHOICES):
    """Map player's choice to a key in the CHOICES dictionary and return the item."""
    pick = pick.lower()
    if pick == "rock":
        pick = 1
    if pick == "paper":
        pick = 2
    if pick == "scissors":
        pick = 3
    pk = CHOICES.get(pick)
    return pk


def whoWon(player, AI):
    """Decide the Winner...
    Rock beats scissors, paper beats rock, and scissors beats paper."""
    p = "player"
    c = "AI"

    # Player win conditions
    if (player == "rock") and (AI == "scissors"):
        return p
    elif (player == "scissors") and (AI == "paper"):
        return p
    elif (player == "paper") and (AI == "rock"):
        return p
    # AI win conditions
    if (player == "scissors") and (AI == "rock"):
        return c
    elif (player == "paper") and (AI == "scissors"):
        return c
    elif (player == "rock") and (AI == "paper"):
        return c
    # Tie condition
    elif player == AI:
        return "tie"
    # IDK... might need to catch exceptions, below.
    else:
        print("Uh oh...")
        return "IDK WHAT HAPPENED"


def check(input, CHOICES):
    """Check if rock, paper, or scissors was input. Anything else ask for the right input."""
    for v in CHOICES.values():
        if v.lower() == input:
            return True
    if input.lower() == "i":
        return True


def main():
    """Main game loop."""
    print("Rock, Paper, Scissors.\n")

    # Variables for count of rounds and scores.
    rounds = 0
    PLAYER_SCORE = 0
    AI_SCORE = 0

    # Impossible mode is off by default.
    IMPOSSIBLE_MODE = False

    # Three choices mapped to integers, 1 through 3.
    CHOICES = {1: "Rock", 2: "Paper", 3: "Scissors"}

    # Infinite game loop.
    while True:
        # Get input with instructions.
        player_pick = input(
            "Rock, Paper, or Scissor. Round {}\n Score Pick or type quit to exit.\n For impossible mode press I.\n".format(
                rounds
            )
        )

        # Toggle impossible mode on and ask for input. Impossible mode must be off in order to turn on.
        if player_pick.capitalize() == "I" and IMPOSSIBLE_MODE == False:
            IMPOSSIBLE_MODE = True
            player_pick = input(
                "Impossible mode enabled. Please pick rock, paper, or scissors.\n"
            )
        # Toggle impossible mode off and ask for input. Impossible mode must be one to turn it off.
        if player_pick.capitalize() == "I" and IMPOSSIBLE_MODE == True:
            IMPOSSIBLE_MODE = False
            player_pick = input(
                "Impossible mode disabled. Please pick rock, paper, or scissors.\n"
            )

        # If the player wants to quit, then quit.
        if player_pick.lower() == "quit":
            exit()
        # Show the score and then ask for input.
        if player_pick.lower() == "score":
            # if the player presses I again here we crash.
            player_pick = input(
                "\nThe score is: Player: {}, AI: {}. \n Now please pick rock, paper or scissors.".format(
                    PLAYER_SCORE, AI_SCORE
                )
            )

        # Check for rock, paper, scissors input and prompt player to correct if not any valid choices.
        while check(player_pick.lower(), CHOICES) != True:
            player_pick = input(
                "The choice: {} was not rock, paper or scissors, please try again.\n You cannot check the score right now.\n".format(
                    player_pick
                )
            )
            # Still quit in this loop.
            if player_pick.lower() == "quit":
                exit()

        # Set the AI choice, and the player choice.
        c = CHOICES[
            AI(CHOICES)
        ]  # Do this only once per loop because its a random choice every time the AI function is called.
        p = player(player_pick, CHOICES)

        # If impossible the "AI" should cheat by looking at the users input.
        # If not impossible "AI" makes random choice.
        if IMPOSSIBLE_MODE == True:
            winner = whoWon(player_pick, impossibleModeAI(player_pick, CHOICES))
        else:
            winner = whoWon(p.lower(), c.lower())

        # Check for winners, print the results, and increment the scores.
        if winner == "player":
            PLAYER_SCORE = PLAYER_SCORE + 1
            print("Player won! {} beats {}".format(p, c))
        elif winner == "AI":
            AI_SCORE = AI_SCORE + 1
            print("AI won! {} beats {}".format(c, p))
        elif winner == "tie":
            print("Tie! Player chose {} and AI chose {}".format(c, p))
        else:
            print("last case")

        # Increment the rounds.
        rounds = rounds + 1
        # Sleep for half a second.
        time.sleep(0.2)


main()
