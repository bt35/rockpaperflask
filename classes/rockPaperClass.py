"""Rock Paper Scissors Game"""
from random import choice
from sys import exit
import time


class RockPaperScissors:
    def __init__(self):

        self.sound = 0
        self.PLAYER_SCORE = 0
        self.AI_SCORE = 0

        # Impossible mode is off by default.
        self.IMPOSSIBLE_MODE = False

        self.CHOICES = {1: "Rock", 2: "Paper", 3: "Scissors"}

    def newGame(self):
        self.PLAYER_SCORE = 0
        self.AI_SCORE = 0
        self.IMPOSSIBLE_MODE = False

    def returnScores(self):

        return (self.PLAYER_SCORE, self.AI_SCORE)

    def AI(self, pick):
        """The AI should pick at random from CHOICES"""
        # Something smells here.. this might be fire.
        pk = choice(list(pick))
        return pk

    def impossibleModeAI(self, player, CHOICES):
        """Stub or impossible mode."""
        # The AI should cheat and read the players choice!
        if player == "rock":
            return "paper"
        if player == "paper":
            return "scissors"
        if player == "scissors":
            return "rock"

    def player(self, pick, CHOICES):
        """Map player's choice to a key in the CHOICES dictionary and return the item."""
        pick = pick.lower()
        if pick == "rock":
            pick = 1
        if pick == "paper":
            pick = 2
        if pick == "scissors":
            pick = 3
        # Should we be handling the game logic in the flask app...?
        pk = CHOICES.get(pick)
        return self.whoWon(pk, self.CHOICES[self.AI(self.CHOICES)])
        # return pk

    def whoWon(self, player, AI):
        """Decide the Winner...
        Rock beats scissors, paper beats rock, and scissors beats paper."""
        p = "player"
        c = "AI"

        # to lower... or this whole thing breakds.
        player = player.lower()
        AI = AI.lower()

        print(player)
        print(AI)

        if self.IMPOSSIBLE_MODE == True:
            AI = self.impossibleModeAI(player, self.CHOICES)
        print(player)
        print(AI)

        # Player win conditions
        if (player == "rock") and (AI == "scissors"):
            self.PLAYER_SCORE += 1
            return p
        elif (player == "scissors") and (AI == "paper"):
            self.PLAYER_SCORE += 1
            return p
        elif (player == "paper") and (AI == "rock"):
            self.PLAYER_SCORE += 1
            return p
        # AI win conditions
        if (player == "scissors") and (AI == "rock"):
            self.AI_SCORE += 1
            return c
        elif (player == "paper") and (AI == "scissors"):
            self.AI_SCORE += 1
            return c
        elif (player == "rock") and (AI == "paper"):
            self.AI_SCORE += 1
            return c
        # Tie condition
        elif player == AI:
            return "tie"
        # IDK... might need to catch exceptions, below.
        else:
            print("Uh oh...")
            return "IDK WHAT HAPPENED, and something is probably broken.\n Who thought Rock Paper Scissors could be so complicated?"

    def check(input, CHOICES):
        """Check if rock, paper, or scissors was input. Anything else ask for the right input."""
        for v in CHOICES.values():
            if v.lower() == input:
                return True
        if input.lower() == "i":
            return True


